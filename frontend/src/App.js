import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import ApolloClient from 'apollo-boost'

// custom components
import PrivateRoute from './PrivateRoute'
import UserProfile from './UserProfile'
import Landing from './Landing'
import Login from './Login'

// styles
import './App.css'

const apolloClient = new ApolloClient({}) // if there is no uri, it directs to /graphql

function App() {
    return (
        <ApolloProvider client={apolloClient}>
            <BrowserRouter>
                <>
                    <Switch>
                        <Route exact path="/" component={Landing} />
                        <Route exact path="/login" component={Login} />
                        <PrivateRoute path="/user" component={UserProfile} />
                    </Switch>
                </>
            </BrowserRouter>
        </ApolloProvider>
    )
}

export default App
