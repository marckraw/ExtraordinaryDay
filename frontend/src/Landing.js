import React from 'react'
import styled from 'styled-components'
import { useTranslation } from 'react-i18next'

const StyledH1 = styled.h1`
    font-size: 24px;
    color: red;
`

const Landing = () => {
    const { t } = useTranslation()
    return (
        <div className="App">
            <header className="App-header">
                <StyledH1>{t('Extraordinary Day')}</StyledH1>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
            </header>
        </div>
    )
}

export default Landing
