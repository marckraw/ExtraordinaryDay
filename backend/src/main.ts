import { NestFactory } from '@nestjs/core'
import * as helmet from 'helmet'
import * as csurf from 'csurf'

import { CompanageMainModule } from './app.module'

async function bootstrap() {
    const app = await NestFactory.create(CompanageMainModule)
    app.use(helmet())
    app.use(csurf())
    app.setGlobalPrefix('api')
    app.enableCors()
    await app.listen(3000)
}
bootstrap()
