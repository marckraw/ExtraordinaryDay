import { Module } from '@nestjs/common'

import { InvoicesController } from './invoices.controller'

@Module({
    imports: [],
    exports: [],
    controllers: [InvoicesController],
    providers: [],
})
export class InvoicesModule {}
