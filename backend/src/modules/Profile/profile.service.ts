import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import * as uuidv1 from 'uuid/v1'

// interfaces
import { Profile } from './interfaces'

@Injectable()
export class ProfileService {
    constructor(
        @InjectModel('Profile') private readonly productModel: Model<Profile>,
    ) {}

    private readonly profiles: Profile[] = [
        {
            id: uuidv1(),
            name: 'Marcin',
            surname: 'Krawczyk',
            age: 28,
        },
        {
            id: uuidv1(),
            name: 'Kasia',
            surname: 'Polewa',
            age: 45,
        },
    ]

    async create(profile: Profile) {
        const newProduct = new this.productModel({
            name: profile.name,
            surname: profile.surname,
            age: profile.age,
        })

        const result = await newProduct.save()

        // this.profiles.push({ ...profile, id: uuidv1() })
        // return [{ status: 'ok' }, ...this.profiles]
        return [{ status: 'ok' }, { id: result.id }]
    }

    remove(id: string) {
        this.profiles.filter(profile => id !== profile.id)
        return [{ status: 'removed' }, ...this.profiles]
    }

    findOne(id: string): Profile {
        return this.profiles.filter(profile => id === profile.id)[0]
    }

    findAll(): Profile[] {
        return this.profiles
    }
}
