export class CreateProfileDto {
    readonly id: string
    readonly name: string
    readonly surname: string
    readonly age: number
}
