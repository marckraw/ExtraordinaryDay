import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { ProfileController } from './profile.controller'
import { ProfileService } from './profile.service'
import { ProfileSchema } from './prodile.model'

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: 'Profile',
                schema: ProfileSchema,
            },
        ]),
    ],
    exports: [],
    controllers: [ProfileController],
    providers: [ProfileService],
})
export class ProfileModule {}
