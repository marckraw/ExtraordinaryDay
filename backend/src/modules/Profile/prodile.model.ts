import * as mongoose from 'mongoose'

export const ProfileSchema = new mongoose.Schema({
    name: { type: String, required: true },
    surname: { type: String, required: true },
    age: { type: Number, required: true },
})
