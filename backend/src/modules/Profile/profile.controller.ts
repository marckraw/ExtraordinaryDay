import {
    Controller,
    Get,
    Req,
    Post,
    Param,
    Body,
    Delete,
    Put,
} from '@nestjs/common'
import { Request } from 'express'

// dto
import { CreateProfileDto } from './dto/create-profile.dto'
import { UpdateProfileDto } from './dto/update-profile.dto'

// services
import { ProfileService } from './profile.service'

// interfaces
import { Profile } from './interfaces'

@Controller('profile')
export class ProfileController {
    constructor(private profileService: ProfileService) {}

    @Post()
    async create(@Body() createProfileDto: CreateProfileDto): Promise<any[]> {
        return this.profileService.create(createProfileDto)
    }

    @Get()
    findAll(@Req() request: Request): Profile[] {
        return this.profileService.findAll()
    }

    @Get(':id')
    findOne(@Param('id') id: string): Profile {
        return this.profileService.findOne(id)
    }

    @Put(':id')
    update(@Param('id') id: string, @Body() updateCatDto: UpdateProfileDto) {
        return `This action updates a #${id} profile`
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.profileService.remove(id)
    }
}
