export interface Profile {
    id: string
    name: string
    surname: string
    age: number
}
