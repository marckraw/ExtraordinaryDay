import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common'

// controllers
import { AppController } from './app.controller'

// modules
import { ProfileModule } from './modules/Profile/profile.module'
import { InvoicesModule } from './modules/Invoices/invoices.module'
import { LoggerMiddleware } from './utils/logger.middleware'

@Module({
    imports: [ProfileModule, InvoicesModule],
    controllers: [AppController],
    providers: [],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(LoggerMiddleware).forRoutes('profile')
    }
}
