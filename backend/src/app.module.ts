import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common'
import { GraphQLModule } from '@nestjs/graphql'
import { MongooseModule } from '@nestjs/mongoose'

// modules
import { ConfigModule } from './config.module'
import { RecipesModule } from './recipes/recipes.module'
import { ProfileModule } from './modules/Profile/profile.module'
import { InvoicesModule } from './modules/Invoices/invoices.module'

import { LoggerMiddleware } from './utils/logger.middleware'

// controllers
import { AppController } from './app.controller'

// services
import { AppService } from './app.service'

@Module({
    imports: [
        ConfigModule,
        RecipesModule,
        GraphQLModule.forRoot({
            debug: true,
            playground: true,
            autoSchemaFile: `schema.gql`,
        }),
        ProfileModule,
        InvoicesModule,
        MongooseModule.forRoot('mongodb://mongo/extraordinary-test-db'),
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class CompanageMainModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(LoggerMiddleware).forRoutes('profile')
    }
}
