import { Controller, Get } from '@nestjs/common'

@Controller()
export class AppController {
    @Get()
    isWorking(): string {
        return 'Working'
    }
}
